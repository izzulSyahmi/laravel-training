<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class BugTracker extends Model
{
    use HasFactory;

    protected $table = "bug_tracker";
    protected $primaryKey = 'id';
    protected $keyType = 'integer';
    public $incrementing = true;
    protected $guarded = [];
    protected $fillable = [];

    public function scopeInsertTracker($query , $newBug)
    {
        try {
            
            DB::beginTransaction();
    
            $query->insert([
                [
                    'title'    => $newBug['title'], 
                    'info'     => $newBug['info'], 
                    'status'   => $newBug['status'], 
                    'severity' => $newBug['severity'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
            ]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollBack();
            return false;
        } 
    }

    public function scopeUpdateTracker($query , $updateBug)
    {
        try {
            
            DB::beginTransaction();
    
            $query->where('id',$updateBug['id'])->update([
                'title'    => $updateBug['title'], 
                'info'     => $updateBug['info'], 
                'status'   => $updateBug['status'], 
                'severity' => $updateBug['severity']
            ]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollBack();
            return false;
        } 
    }

    public function scopeDeleteTracker($query , $id)
    {
        try {
            
            DB::beginTransaction();
    
            $query->where('id',$id)->delete();

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollBack();
            return false;
        } 
    }
}
