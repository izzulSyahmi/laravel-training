<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributesValues extends Model
{
    use HasFactory;
    
    protected $table = "attributes_value";
    protected $primaryKey = 'id';
    protected $keyType = 'true';
    public $incrementing = false;
    protected $guarded = [];
    protected $fillable = [];

    public function scopeStatus($query) {
        return $query->where('attributes_id',1)
            ->orderBy('code', 'asc')
            ->get();
    }

    public function scopeSeverity($query) {
        return $query->where('attributes_id',2)
            ->orderBy('code', 'asc')
            ->get();
    }

    public function scopeSelectAttribute($query , $type , $code )
    {
        $selected = $query->where('attributes_id',$type)
            ->where("code" , $code)->first();

        return !empty($selected) ? $selected->value : "value";
    }
}
