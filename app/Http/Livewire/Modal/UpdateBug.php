<?php

namespace App\Http\Livewire\Modal;

use App\Models\BugTracker;
use Livewire\Component;

class UpdateBug extends Component
{
    protected $listeners = [
        'mountModal'
    ];

    public $selectedBug;

    protected $rules = [
        'selectedBug.title' => 'required',
        'selectedBug.info' => 'required',
        'selectedBug.status' => 'required',
        'selectedBug.severity' => 'required',
    ];

    public function mountModal($id)
    {
        $this->selectedBug = BugTracker::where("id",$id)->first()->toArray();
    }

    public function render()
    {
        return view('livewire.modal.update-bug');
    }

    public function updateBug()
    {
        try {

            $this->validate();
            $condition = BugTracker::UpdateTracker($this->selectedBug);

            if($condition)
            {
                $this->emit('refreshComponent');
                $this->emit('hideModal', 'modalupdateBug');
            }

        } catch (Exception $e) {
            
        } 

    }
}
