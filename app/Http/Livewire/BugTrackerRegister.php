<?php

namespace App\Http\Livewire;

use App\Models\BugTracker;
use Illuminate\Validation\Rule;

use Livewire\Component;

class BugTrackerRegister extends Component
{
    public $bug;

    protected  $rules = [
        'bug.title' => 'required',
        'bug.info' => 'required',
        'bug.status' => 'required',
        'bug.severity' => 'required',
    ];

    public function mount()
    {
        $this->bug = [
            "title" => "",
            "info" => "",
            "status" => "ST01", // value status open
            "severity" => "",
        ];
    }

    public function render()
    {
        return view('livewire.bug-tracker-register');
    }

    public function registerBug()
    {
        try {

            $this->validate();
            $newBug = BugTracker::InsertTracker($this->bug);

            return redirect()->to('/bug-tracker-list');

        } catch (Exception $e) {
            
        } 
    }
}
