<?php

namespace App\Http\Livewire;

use App\Models\BugTracker;

use Livewire\Component;
use Livewire\WithPagination;

class BugTrackerList extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    protected $listeners = [
        'refreshComponent' => '$refresh'
    ];

    public $selectedBug;

    protected $rules = [
        'selectedBug.title' => 'required',
        'selectedBug.info' => 'required',
        'selectedBug.status' => 'required',
        'selectedBug.severity' => 'required',
    ];

    public function render()
    {
        // return view('livewire.bug-tracker-list',[
        //     "bugList" => BugTracker::orderby('status', 'asc')->paginate(5)
        // ]);

        return view('livewire.bug-tracker-list',[
            "bugList" => []
        ]);
    }

    public function deleteBug($id)
    {
        BugTracker::DeleteTracker($id);
    }

    public function modalUpdateBug($id)
    {
        $this->selectedBug = BugTracker::where("id",$id)->first()->toArray();

        //@include modal
        $this->emit('showModal', 'modalupdateBug');

        //@livewire modal only
        // $this->emit('mountModal',$id);
    }

    public function updateBug()
    {
        try {

            $this->validate();
            $condition = BugTracker::UpdateTracker($this->selectedBug);

            if($condition)
            {
                $this->emit('hideModal', 'modalupdateBug');
            }

        } catch (Exception $e) {
            
        } 
    }
}
