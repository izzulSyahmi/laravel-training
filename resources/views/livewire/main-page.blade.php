<div>
    <div class="container">
        <div class="row">
          <div class="col-lg-6 d-flex flex-column justify-content-center">
            <h1 data-aos="fade-up" class="aos-init aos-animate">Laravel Livewire Training</h1>
            <h2 data-aos="fade-up" data-aos-delay="400" class="aos-init aos-animate"></h2>
            <div data-aos="fade-up" data-aos-delay="600" class="aos-init aos-animate">
                <div class="text-center text-lg-start">
                    <a href="{{ url('/bug-tracker-list') }}"
                      class="btn btn-primary font-weight-bolder ">
                      Bug Tracker
                      <i class="bi bi-arrow-right"></i>
                    </a>
                </div>
            </div>
          </div>
          <div class="col-lg-6 hero-img aos-init aos-animate" data-aos="zoom-out" data-aos-delay="200">
            <img class="img-fluid sprmbuilding" src="https://estk.sprm.gov.my/assets/media/bg/sprm-building.webp" alt="First slide">
          </div>
        </div>
    </div>
</div>
