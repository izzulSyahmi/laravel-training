<div>
    <div class="card card-custom gutter-b mt-0">
        <div class="card-header">
            <h3 class="card-title text-dark-75 font-weight-bolder">New Bug</h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <span class="example-toggle" data-toggle="tooltip" title="" data-original-title="View code"></span>
                    <span class="example-copy" data-toggle="tooltip" title="" data-original-title="Copy code"></span>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="">

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Title </label>
                    <div class="col-lg">
                        <input type="text" class="form-control form-control-sm form-control-solid @error('bug.title') is-invalid @enderror"
                            placeholder="" wire:model.lazy="bug.title">
                        @error('bug.title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Info</label>
                    <div class="col-lg">
                        <textarea class="form-control form-control-sm form-control-solid @error('bug.info') is-invalid @enderror"
                        wire:model.lazy="bug.info"
                        placeholder=""
                        ></textarea>

                        @error('bug.info')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Status</label>
                    <div class="col-lg">
                        <select
                            class="form-control form-control-sm form-control-solid custom-select"
                            {{-- @error('ptb_f003jenispermohonan') is-invalid @enderror"" --}}
                            wire:model.lazy="bug.status" disabled
                            >

                            {{-- <option value="" selected="selected">-- Please Select--</option> --}}
                            @foreach(App\Models\AttributesValues::Status() as $status)
                            <option value="{{ $status->code }}">
                                {{ $status->value }}
                            </option>
                            @endforeach

                        </select>

                        {{-- @error('ptb_f003jenispermohonan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror --}}
                    </div>
                </div>

                <div class="form-group row mb-2">
                    <label class="col-lg-3 col-form-label font-weight-bolder">Severity</label>
                    <div class="col-lg">
                        <select
                            class="form-control form-control-sm form-control-solid custom-select
                            @error('bug.severity') is-invalid @enderror"
                            wire:model.lazy="bug.severity"
                            >

                            <option value="" selected="selected">-- Please Select--</option>
                            @foreach(App\Models\AttributesValues::Severity() as $severity)
                            <option value="{{ $severity->code }}">
                                {{ $severity->value }}
                            </option>
                            @endforeach

                        </select>

                        @error('bug.severity')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

            </div>
        </div>
        <div class="card-footer text-right bg-gray-100 border-top-0">
            <button wire:click.prevent="registerBug"
                class="btn btn-primary font-weight-bolder font-size-sm">

                Register
            </button>
        </div>
    </div>
</div>
