<div wire:ignore.self class="modal fade" data-backdrop="static" id="modalupdateBug" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder text-dark-75" id="exampleModalLabel">
                    Update Bug Tracker
                </h5>
            </div>
            <div class="modal-body">
                @if(!empty($selectedBug))
            
                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label font-weight-bolder">Title </label>
                        <div class="col-lg">
                            <input type="text" class="form-control form-control-sm form-control-solid @error('selectedBug.title') is-invalid @enderror"
                                placeholder="" wire:model.lazy="selectedBug.title">
                            @error('selectedBug.title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label font-weight-bolder">Info</label>
                        <div class="col-lg">
                            <textarea class="form-control form-control-sm form-control-solid @error('selectedBug.info') is-invalid @enderror"
                            wire:model.lazy="selectedBug.info"
                            placeholder=""
                            ></textarea>
    
                            @error('selectedBug.info')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label font-weight-bolder">Status</label>
                        <div class="col-lg">
                            <select
                                class="form-control form-control-sm form-control-solid custom-select @error('selectedBug.status') is-invalid @enderror"
                                wire:model.lazy="selectedBug.status"
                                >
    
                                @foreach(App\Models\AttributesValues::Status() as $status)
                                <option value="{{ $status->code }}">
                                    {{ $status->value }}
                                </option>
                                @endforeach
    
                            </select>
    
                            @error('selectedBug.status')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
    
                    <div class="form-group row mb-2">
                        <label class="col-lg-3 col-form-label font-weight-bolder">Severity</label>
                        <div class="col-lg">
                            <select
                                class="form-control form-control-sm form-control-solid custom-select
                                @error('selectedBug.severity') is-invalid @enderror"
                                wire:model.lazy="selectedBug.severity"
                                >
    
                                @foreach(App\Models\AttributesValues::Severity() as $severity)
                                <option value="{{ $severity->code }}">
                                    {{ $severity->value }}
                                </option>
                                @endforeach
    
                            </select>
    
                            @error('selectedBug.severity')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer text-right bg-gray-100 border-top-0">
                        <button wire:click.prevent="updateBug"
                            class="btn btn-success font-weight-bolder font-size-sm">
            
                            Update
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>