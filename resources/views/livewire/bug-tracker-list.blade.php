
<div class="container-fluid">
    <div class="card-header border-0 pt-3">
        <h3 class="card-title align-items-start flex-column mt-7">
            <span class="card-label font-weight-bolder text-dark-75">
                Bug Tracker List
            </span>
        </h3>
        <div class="card-toolbar pt-3" style="display:block" >
            <a href="{{ route('bug-tracker-register') }}"
                class=" float-right btn btn-primary font-weight-bolder font-size-sm">
                Register New Bug
            </a>
        </div>
    </div>

    @include('livewire.modal.update-bug')
    {{-- @livewire('modal.update-bug') --}}

    <div class="card-body pt-5 pt-5">
        <div class="table-responsive ma-5">
            <table class="table table-head-custom table-vertical-center">
                <thead>
                    <tr class="text-left">
                        <th style="">No.</th>
                        <th >
                            Title
                        </th>
                        <th >
                            Info
                        </th>
                        <th width="100px">
                            Status
                        </th>
                        <th width="100px">
                            Severity
                        </th>
                        <th width="150px">
                            Date
                        </th>
                        <th class="pr-0 text-right" width="200px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($bugList as $indexKey => $bug )
                    <tr>
                        <td >
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                {{-- {{ ($bug->currentpage()-1) * $bug->perpage() + $indexKey + 1  }} --}}
                                {{ $indexKey + 1 }}
                            </span>
                        </td>
                        <td>
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                {{ $bug->title }}
                            </span>
                        </td>
                        <td>
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                {{ $bug->info }}
                            </span>
                        </td>
                        <td>
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                {{ App\Models\AttributesValues::SelectAttribute(1,$bug->status) }}
                            </span>
                        </td>
                        <td>
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                {{ App\Models\AttributesValues::SelectAttribute(2,$bug->severity) }}
                            </span>
                        </td>
                        <td>
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                            </span>
                        </td>
                        <td>
                            <button wire:click.prevent="modalUpdateBug({{$bug->id}})"
                                class="btn btn-success font-weight-bolder ">
                                Update
                            </button>

                            <button wire:click.prevent="deleteBug({{$bug->id}})"
                                class="btn btn-danger font-weight-bolder ">
                                Delete
                            </button>
                        </td>
                    </tr>

                    @empty
                    <tr class="text-center mt-2 mb-2">
                        <td colspan="6" class="" valign="top">
                            <span class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">
                                Tiada data
                            </span>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            @if(!empty($bugList))
                {{ $bugList->links() }}
            @endif

            {{-- <div wire:loading>
                <div id="loading" class="loading mx-auto" style="display:block;">
                    <img class="loading-image" src="{{ asset('assets/media/loader/loader.svg') }}" alt="loader">
                </div>
            </div> --}}
        </div>
    </div>

</div>
