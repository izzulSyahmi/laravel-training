<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\MainPage;
use App\Http\Livewire\BugTrackerList;
use App\Http\Livewire\BugTrackerRegister;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',
    MainPage::class
)->name('main-page');

Route::get('/bug-tracker-list',
    BugTrackerList::class
)->name('bug-tracker-list');


Route::get('/bug-tracker-register',
   BugTrackerRegister::class
)->name('bug-tracker-register');

