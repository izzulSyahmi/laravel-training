<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class AttributesValue extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if table users profile is empty
        if(DB::table('attributes_value')->get()->count() == 0){

            DB::table('attributes_value')->insert([
                [
                    'id' => 1,
                    'value' => 'Open',
                    'code' => 'ST01',
                    'attributes_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 2,
                    'value' => 'In Progress',
                    'attributes_id' => 1,
                    'code' => 'ST02',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 3,
                    'value' => 'Close',
                    'attributes_id' => 1,
                    'code' => 'ST03',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 4,
                    'value' => 'Low',
                    'code' => 'SV01',
                    'attributes_id' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 5,
                    'value' => 'Medium',
                    'code' => 'SV02',
                    'attributes_id' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 6,
                    'value' => 'High',
                    'code' => 'SV03',
                    'attributes_id' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
            ]);

        }
    }
}
